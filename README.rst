==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

========
Pop Conf
========

Pop-conf a legacy tool to allow for creating and app-merging
configuration options for `pop` projects. Plugin Oriented Programming
presents a means to merge multiple applications together dynamically.
This capabilities requires that the startup of these applications needs
to be managed from within the programming paradigm.
`pop-config` is a more advanced version of this tool and should be used instead.

Pop-conf is not just about being able to work with `pop` projects
to facilitate app-merging, it has also been designed to make the startup
of an application much simpler, and to make the expensive boiler plate
of startup and configuration as transparent as possible. When making
`pop` projects the idea is that projects are developed in very small
chunks and are then dynamically merged together. So the creation of
these mergeable apps needs to be quick and easy!

Pop-conf also solves a problem with configuration of applications,
when making a new application, if you want robust configuration
loading, you need to be able to take options from the command line,
environment variables, and configuration files. Pop-conf does all
of this for you, and loads them in the correct order, all behind the
scenes, making your life easier.

Understanding the conf.py
=========================

Pop relies on a configuration file to manage how to merge apps, and
also how to manage and merge configuration data. The data in this file
is presented in 4 Python dictionaries and defines every aspect of
configuration loading.

The 4 dictionaries are called *CONFIG*, *CLI_CONFIG*, *SUBCOMMANDS*
and *DYNE*. Each dictionary serves a specific purpose. Between them
you can define how the cli arguments are presented, all configuration
defaults, documentation, etc.

The CONFIG Dictionary
=====================

The bulk of the configuration will be present in the *CONFIG* dictionary.
all of your configuration options are defined here. Most entries in the
*CONFIG* dictionary will be very simple and just expose the most basic
values:

.. code-block:: python

    CONFIG = {
        "name": {
            "default": "frank",
            "help": "Enter the name to use",
        },
    }

This simple example presents the documentation for the configuration value
and what the default value should be.

Vertically app-merged projects can add config items to their parent dynes like so:

.. code-block:: python

    CONFIG = {
        "new_item": {
            "type": int,
            "default": 1,
            "dyne": "idem",
        },
    }

Many more options can be used, but they will be covered in the reference
document.

The CLI_CONFIG Dictionary
=========================

Adding a configuration value does not make it appear on the command line.
Each application can be extended to include command line options.
Lets extend our earlier example to expose the "name" option as a command
line argument:

.. code-block:: python

    CLI_CONFIG = {
        "name": {},
    }
    CONFIG = {
        "name": {
            "default": "frank",
            "help": "Enter the name to use",
        },
    }

That's it! The "name" option is now available on the command line and can
be used as `--name bob`.

But what if we want it to be a positional argument? Simple! Just add the
positional option to the *CLI_CONFIG*:

.. code-block:: python

    CLI_CONFIG = {
        "name": {
            "positional": True,
        },
    }
    CONFIG = {
        "name": {
            "default": "frank",
            "help": "Enter the name to use",
        },
    }

You can inherit cli args from another project.  Say, for example that you want to implement the `--output` flag
exactly the same way `rend` does, you can source it like this:

.. code-block:: python

    CLI_CONFIG = {
        "output": {
            "source": "rend",
        },
    }

Many more options exist that allow you to control every aspect of the user's
command line experience.

The SUBCOMMANDS Dictionary
==========================

Sometimes it is desirable to have subcommands. Subcommands allow your CLI
to work in a way similar to the git cli, where you have multiple routines
that all can be called from a single command.

Lets add a few more things to our example so that we can have subcommands.

.. code-block:: python

    CLI_CONFIG = {
        "name": {
            "subcommands": ["test", "apply"],
        },
        "weight": {},
        "power": {
            "subcommands": ["apply"],
        },
    }
    CONFIG = {
        "name": {
            "default": "frank",
            "help": "Enter the name to use",
        },
        "weight": {
            "default": "150",
            "help": "Enter how heavy it should be",
        },
        "power": {
            "default": "100",
            "help": "Enter how powerful it should be",
        },
    }

    SUBCOMMANDS = {
        "test": {
            "help": "Used to test",
            "desc": "When running in test mode, things will be tested",
        },
        "apply": {
            "help": "Used to apply",
            "desc": "When running in apply mode, things will be applied",
        },
    }


In this example we see that the option `name` will be available under
the subcommands `test` and `apply`. The option `power` will be available
only under the subcommand `apply` and the option `weight` is globally
available.

The DYNE Dictionary
===================

The *DYNE* dictionary allows you to control what dynamic names your app is
presenting to other `pop` projects. This name gets used not only inside
of `pop-conf` but also inside of `pop` to determine what plugin subsystems
this application merges with. The *DYNE* system allows for your cli to be
extended by third party code, enabling configuration options to be made
available to your application via external code.

The *DYNE* system is very powerful. But since it is not critical to getting
started with `pop-conf` it will be covered in more depth in another document.
